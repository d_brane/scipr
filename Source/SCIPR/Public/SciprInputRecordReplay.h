// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

//#include "Engine/Engine.h" 

#include "CoreMinimal.h"

#include "Runtime/Json/Public/Json.h"

#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Interfaces/IHttpRequest.h"
#include "Runtime/Engine/Classes/Components/SceneComponent.h"

#include "Components/ActorComponent.h"

#include "SciprInputRecordReplay.generated.h"

//File specification:
//{
//"recording_name": "Test_1",
//"recording_frequency": 30,
//"length": 3.1,	// highest timestep
//
//
//"action_events":[
//	{ "timestamp": 0.100, "ids": [ ... ] },
//	{ "timestamp": 0.133, "ids": [ {"id": "left_trigger", "value": true }, ... ] },
//	...
//	// or
//	{ "timestamp": 0.100, "id": "left_trigger", "value": true },
//	{
//],
//"transform_events":[
//	{ "timestamp": 0.100, "transforms": [
//		{ "id": "head", "isRelative": false, "value": { "pos": [20.1,0.0,1.0], "rot": [0.55,1.0,20.0], "scale": [1.0,1.0,1.0] } },
//	]},
//	{ "timestamp": 0.133, "transforms": [ ... ] },
//	...
//]
//
//}


//updated filespec:
//header {
//	length: 12 (sec),
//	buffer_length: 4000 (ms),
//	interleaved: true / false / split,
//	binary_buffers: true / false / embedded,

//}


//- allow interleaved writing for streaming (bool interleaved = true / false)
//- header { interleaved { true, false, split }
//-- if true: 
//	mix events and transforms as a stream (where events interrupt transforms), thereby slightly increasing filesize
//	and making it harder to filter unneeded data but allowing for streamable data (over network and so on)
//--- in this mode, have option to use multiple files, for each event and transform type. this again allows streaming {option: split}
//--- naming: recording_name.json { recording_name_transform_XX.json, recording_name_events.json }
//-- if false:
//	seperate out transform events (hands movments and so on) and actual events.
//	allows for better compression, and easier filtering (only gets transforms or only get events, events can also be seperated)

//- each tracked object a (bin) file: (head, hands, torso, ...)
//-- contain:
//--- compression type (lossy,lossless) - bezier simplification and so on, or simple RLE
//--- vec3 (pos), vec4 (quat)
//--- timestamps
//-- have important positions (bound by actions) that are not interpolated, but fixed - need for example when clickin on UI using laser pointer type input
//-- should be streamable streamable

//- RLE for simple lossless compress, zip everything up again using zlib or smthg

//struct TimeStampedPosRot { union { float timestamp; vec3 pos; vec4 rot; } }; // bool essential_point for action stamps ?? not sure if needed

//struct TrackedObject {
//	enum CompressionType { RLE, BEZIER, UNCOMPRESSED, ADPCM, ... } compression_type;
//	// int sample_rate // just do every frame and apply compresseion ?? maybe better
//	string id;
//	float track_length;
//	array<TimeStampedPosRot> values;
//};

//struct BoolAction {}
//struct FloatAction {}
////or each frame:
//struct 6DofState { vec3f pos; quat rot; }
//struct RiftState { float at_time; bool A; bool B; bool Thump; vec2f thumbpad; float TriggerA; float TriggerB; }
//struct ViveState { float at_time; bool A; bool Grap; bool Thump; vec2f thumbpad; float Trigger; }
//struct OculusGoState { float at_time; bool A; bool Thump; vec2f thumbpad; float Trigger; }

//per minute cost (uncompressed):
//- Oculus
//-- sizeof(6DofState) => 3*32 + 4*32 = 224 bit * 3      = 672 bit / frame (2 hands + head)
//-- sizeof(RiftState) => 32+1+1+1+64+32+32 = 164bit * 2 = 328 bit / frame
//-- uncompressed:                                       -----------------
//--                                                      1000 bit / frame
//-- 
//-- 90 fps => 90000 bit / sec        => 10.9 KB/s
//-- 1 min of uncompressed recording :=> 0.64 MB/min


//maybe build buffered structures:

//struct RiftState { int frames_per_sec = 90; int buffer_number_of_frames = 90;  bool A[buffer_nr_of_frames]; bool B[...]; .... }
//--> then go on compressed_state = compress_chunk(RiftState) and serialize_chunk(compressed_state)

//=====================================================================

//struct ActionEvent<T> { char id = 'HELO'; double at_time; bool essential_action; vec3 at_pos; quat with_rot; vec3f velocity; T payload; }

//struct 6DofState { double at_time; vec3f at_pos; quat with_rot; }


//=====================================================================

//struct HandState { vec3f ankle_pos; quat ankle_rot; vec3f finger_pos[5]; } (sizeof(3*32 + 4*32 + 5*3*32) == 704)

//-- sizeof(2 * HandState) => 1408 bit / frame
//-- 90 fps                => 15.46 KB / sec (126720 bit / sec)
//-- 1 min of uncompressed =>  0.91 MB / min




// https://forums.unrealengine.com/showthread.php?45027-Best-Way-to-Handle-JSON
// https://answers.unrealengine.com/questions/341767/how-to-add-an-array-of-json-objects-to-a-json-obje.html

// https://answers.unrealengine.com/questions/457211/binding-c-events-to-event-dispatcher.html
// https://wiki.unrealengine.com/Delegates_In_UE4,_Raw_Cpp_and_BP_Exposed
// https://forums.unrealengine.com/showthread.php?61599-C-Event-dispatchers-How-do-we-implement-them

// https://answers.unrealengine.com/questions/52444/property-replication-in-component.html

UENUM(BlueprintType)
enum class ESciprHandPrepend: uint8
{
	None 	UMETA(DisplayName = "None"),
	Left 	UMETA(DisplayName = "Left_"),
	Right	UMETA(DisplayName = "Right_")
};

USTRUCT(BlueprintType)
struct FSciprActionEvent {
	GENERATED_BODY()

	UPROPERTY()
	double TimeStamp;
	UPROPERTY(BlueprintReadWrite)
	FString Id;
	UPROPERTY(BlueprintReadWrite)
	bool Value;

	FSciprActionEvent() { TimeStamp = 0.0;  Id = "default"; Value = false; }
	FSciprActionEvent(double AtTime, FString Identifier, bool EventValue) { TimeStamp = AtTime; Id = Identifier; Value = EventValue; }
};

USTRUCT(BlueprintType)
struct FSciprNumberEvent {
	GENERATED_BODY()

	UPROPERTY()
	double TimeStamp;
	UPROPERTY(BlueprintReadWrite)
	FString Id;
	UPROPERTY(BlueprintReadWrite)
	float Value;
	UPROPERTY(BlueprintReadWrite)
	bool State;

	FSciprNumberEvent() { TimeStamp = 0.0;  Id = "default"; Value = 0.0f; State = false; }
	FSciprNumberEvent(double AtTime, FString Identifier, float EventValue, bool ActionState) { TimeStamp = AtTime; Id = Identifier; Value = EventValue; State = ActionState; }
};

USTRUCT(BlueprintType)
struct FSciprStringEvent {
	GENERATED_BODY()

	UPROPERTY()
	double TimeStamp;
	UPROPERTY(BlueprintReadWrite)
	FString Id;
	UPROPERTY(BlueprintReadWrite)
	FString Value;
	UPROPERTY(BlueprintReadWrite)
	bool State;

	FSciprStringEvent() { TimeStamp = 0.0;  Id = "default"; Value = "1"; State = false; }
	FSciprStringEvent(double AtTime, FString Identifier, FString EventValue, bool ActionState) { TimeStamp = AtTime; Id = Identifier; Value = EventValue; State = ActionState; }
};

USTRUCT(BlueprintType)
struct FSciprTransformEvent {
	GENERATED_BODY()

	UPROPERTY()
	double TimeStamp;
	UPROPERTY(BlueprintReadWrite)
	FString Id;
	UPROPERTY(BlueprintReadWrite)
	FTransform Value;
	UPROPERTY(BlueprintReadWrite)
	bool IsRelative;

	FSciprTransformEvent() { TimeStamp = 0.0; Id = "default"; Value = FTransform(); IsRelative = false; }
	FSciprTransformEvent(double AtTime, FString Identifier, FTransform Transform, bool IsRelativeTransform) { TimeStamp = AtTime; Id = Identifier; Value = Transform; IsRelative = IsRelativeTransform; }
};


//DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FActionEventFiredDelegate, )
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSciprStartRecordingDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSciprStopRecordingDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSciprStartPlayback);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSciprStopPlayback);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSciprFileLoaded);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSciprActionEventFiredDelegate, FString, Identifier, bool, Value);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FSciprNumberEventFiredDelegate, FString, Identifier, float, Value, bool, State);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FSciprStringEventFiredDelegate, FString, Identifier, FString, Value, bool, State);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FSciprTransformEventFiredDelegate, FString, Identifier, FTransform, Value, bool, IsRelative);



UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class SCIPR_API USciprInputRecordReplay : public UActorComponent
{
	GENERATED_BODY()

protected:
	TSharedPtr<FJsonObject> MainJsonObject;
	double StartTime;
	double EndTime;
	double CurrentTime;

	double TimeSinceLastInterpolationStep;

	int CurrentActionIndex;
	int CurrentNumberIndex;
	int CurrentStringIndex;
	int CurrentTransformIndex;
	int CurrentTrackedTransformIndex;

	FTimerHandle RecordTrackedObjectsTimerHandle;

	void RequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded);
	bool LoadFromString(FString Data);
public:
	UPROPERTY(BlueprintReadOnly)
	FString Name;
	UPROPERTY(BlueprintReadOnly)
	FString Filepath;
	UPROPERTY(BlueprintReadOnly)
	int PullingFrequency;
	
	UPROPERTY(BlueprintReadOnly)
	bool IsRecording;
	UPROPERTY(BlueprintReadOnly, Replicated)
	bool IsPlaying;
	UPROPERTY(BlueprintReadOnly)
	bool IsTracking;

	UPROPERTY(BlueprintReadWrite)
	bool IsNetworked;
	UPROPERTY(BlueprintReadWrite)
	bool IsListening;

	UPROPERTY(BlueprintReadWrite)
	bool AutoTransformTrackedObjects;
	UPROPERTY(BlueprintReadWrite)
	bool InterpolateTrackedObjectMovements;
	UPROPERTY(BlueprintReadWrite)
	float ReplaySpeed;

	UPROPERTY(BlueprintReadOnly)
	TMap<FString, USceneComponent *> TrackedObjects;

	UPROPERTY(BlueprintReadOnly, Replicated)
	TArray<FSciprActionEvent> ActionEvents;
	UPROPERTY(BlueprintReadOnly, Replicated)
	TArray<FSciprNumberEvent> NumberEvents;
	UPROPERTY(BlueprintReadOnly, Replicated)
	TArray<FSciprStringEvent> StringEvents;
	UPROPERTY(BlueprintReadOnly, Replicated)
	TArray<FSciprTransformEvent> TransformEvents;
	UPROPERTY(BlueprintReadOnly)
	TArray<FSciprTransformEvent> TrackedTransformEvents;

public:
	// Sets default values for this component's properties
	USciprInputRecordReplay();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void StartRecording(FString RecordingName = "InputRecording", float TransformPullingFrequency = 30, FString Path = "./");
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void StopRecording();

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void LoadFile(FString File, FString Path = "/", bool StartPlaybackAfterLoading = false);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void LoadFileFromURL(FString URL);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void StartPlayback();
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void StopPlayback();
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual float GetCurrentTime() const;
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual float GetEndTime() const;

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddActionEvent(FString Identifier, bool Value);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddActionEventPrepended(FString Identifier, bool Value, FString Prepend);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddNumberEvent(FString Identifier, float Value, bool State);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddStringEvent(FString Identfier, FString Value, bool State);
	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddTransformEvent(FString Identfier, FTransform Value, bool IsRelativeTransform);

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	virtual void AddTrackedObject(FString Identifier, USceneComponent *ObjectToTrack);
	//UFUNCTION(BlueprintCallable, Category = "DancingMeshes|InputRecorder")
	//virtual void RemoveTrackedObject(FString Identifier);
	virtual void RecordTrackedObjects();


	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprStartRecordingDelegate OnStartRecording;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprStopRecordingDelegate OnStopRecording;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprStartPlayback OnStartPlayback;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprStopPlayback OnStopPlayback;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprFileLoaded OnFileLoaded;

	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprActionEventFiredDelegate OnActionEventFired;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprNumberEventFiredDelegate OnNumberEventFired;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprStringEventFiredDelegate OnStringEventFired;
	UPROPERTY(BlueprintAssignable, Category = "DancingMeshes|InputRecorder")
	FSciprTransformEventFiredDelegate OnTransformEventFired;
};
