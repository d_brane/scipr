// Fill out your copyright notice in the Description page of Project Settings.

#include "SciprInputRecordReplay.h"


#include "Runtime/Json/Public/Serialization/JsonWriter.h"
#include "Runtime/Json/Public/Policies/PrettyJsonPrintPolicy.h"
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"
#include "Runtime/JsonUtilities/Public/JsonObjectConverter.h"
#include "Runtime/Engine/Public/TimerManager.h"

#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Core/Public/Misc/Paths.h"

#include "Runtime/Online/HTTP/Public/Http.h"
#include "Runtime/Online/HTTP/Public/Interfaces/IHttpResponse.h"
#include "Runtime/Online/HTTP/Public/Interfaces/IHttpRequest.h"

#include "Kismet/KismetMathLibrary.h"

#include "Net/UnrealNetwork.h"


// Sets default values for this component's properties
USciprInputRecordReplay::USciprInputRecordReplay()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	this->SetComponentTickEnabled(false);

	//[todo][fixme] had to uncomment both for deployment in 4.24, not sure if this breaks replication
	//SetIsReplicated(true);
	//bReplicates = true;

	MainJsonObject = MakeShareable(new FJsonObject);

	IsRecording = false;
	IsPlaying = false;
	IsTracking = false;

	IsNetworked = false;
	IsListening = false;

	AutoTransformTrackedObjects = true;
	InterpolateTrackedObjectMovements = true;
	TimeSinceLastInterpolationStep = 0.0;

	ReplaySpeed = 1.0f;
	StartTime = 0.0;
	EndTime = 0.0;
	CurrentTime = 0.0;
	CurrentActionIndex = 0;
	CurrentNumberIndex = 0;
	CurrentStringIndex = 0;
	CurrentTransformIndex = 0;
	CurrentTrackedTransformIndex = 0;

}


// Called when the game starts
void USciprInputRecordReplay::BeginPlay()
{
	Super::BeginPlay();

	// ...

}


void USciprInputRecordReplay::GetLifetimeReplicatedProps(TArray< class FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(USciprInputRecordReplay, IsPlaying);
	//DOREPLIFETIME(UInputRecordReplay, IsTracking);
	//DOREPLIFETIME(UInputRecordReplay, PullingFrequency);

	DOREPLIFETIME(USciprInputRecordReplay, ActionEvents);
	DOREPLIFETIME(USciprInputRecordReplay, NumberEvents);
	DOREPLIFETIME(USciprInputRecordReplay, StringEvents);
	DOREPLIFETIME(USciprInputRecordReplay, TransformEvents);
	//DOREPLIFETIME(UInputRecordReplay, TrackedTransformEvents);
	//DOREPLIFETIME(USystemAbilityManagerComponent, LeftHandAction);
	//DOREPLIFETIME_CONDITION(USystemAbilityManagerComponent, AbilityList, COND_OwnerOnly);
}


// Called every frame
void USciprInputRecordReplay::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//if (GEngine)
	//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Ticking!"));
	//double speed = (double)ReplaySpeed;
	//double currentTime = (FPlatformTime::Seconds() - StartTime) * speed;
	// not happy with currentTime.
	// instead do: (not depending on platform time, and speed can be changed while playback)
	//CurrentTime += DeltaTime * speed;
	//double currentTime = CurrentTime;
	// still doesn't support skipping - as this would mean state problems,
	// maybe reference hand/head transforms, when executing Event (maybe as performance option,
	// or enable, during seeking/fast forward).
	// currently actions are linearly executed, which could create troubles, when action
	// is called, before hand is on right position, for action.

	if (IsPlaying) {

		if (TimeSinceLastInterpolationStep >= (1.0 / (double)PullingFrequency)) {
			TimeSinceLastInterpolationStep = 0.0;
		}
		TimeSinceLastInterpolationStep += DeltaTime;

		double speed = (double)ReplaySpeed;
		CurrentTime += DeltaTime * speed;
		double currentTime = CurrentTime;

		//if (GEngine)
		//	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("IS PLAYING"));
		//double highestStamp;
		//double highestTimeStamp = FMath::Max(TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp,
		//		ActionEvents[CurrentActionIndex].TimeStamp);
		//double highestTimeStamp = FMath::Max3(TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp,
		//	ActionEvents[CurrentActionIndex].TimeStamp,
		//	NumberEvents[CurrentNumberIndex].TimeStamp);
		//double highestTimeStampOfTwo = FMath::Max(StringEvents[CurrentStringIndex].TimeStamp, TransformEvents[CurrentTransformIndex].TimeStamp);
		//highestTimeStamp = FMath::Max(highestTimeStamp, highestTimeStampOfTwo);

		//while (currentTime > highestTimeStamp) {
		//	// move tracked objects
		//	if ((CurrentTrackedTransformIndex < TrackedTransformEvents.Num())
		//		&& (currentTime > TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp)) {

		//		FSciprTransformEvent tracked_data = TrackedTransformEvents[CurrentTrackedTransformIndex];

		//		if (InterpolateTrackedObjectMovements) {
		//			FTransform target = tracked_data.Value;
		//			FTransform current = TrackedObjects[tracked_data.Id]->GetRelativeTransform();
		//			FTransform result = UKismetMathLibrary::TInterpTo(current, target, DeltaTime, (float)PullingFrequency);
		//			TrackedObjects[tracked_data.Id]->SetRelativeTransform(result);
		//		}
		//		else {
		//			TrackedObjects[tracked_data.Id]->SetRelativeTransform(tracked_data.Value);
		//		}

		//		CurrentTrackedTransformIndex++;
		//	}

		//	// fire action events
		//	if ((CurrentActionIndex < ActionEvents.Num()) && (currentTime > ActionEvents[CurrentActionIndex].TimeStamp)) {
		//		OnActionEventFiredDelegate.Broadcast(ActionEvents[CurrentActionIndex].Id, ActionEvents[CurrentActionIndex].Value);
		//		CurrentActionIndex++;
		//	}

		//	//// fire number events
		//	//if ((CurrentNumberIndex < NumberEvents.Num()) && (currentTime > NumberEvents[CurrentNumberIndex].TimeStamp)) {
		//	//	OnNumberEventFiredDelegate.Broadcast(NumberEvents[CurrentNumberIndex].Id, NumberEvents[CurrentNumberIndex].Value, NumberEvents[CurrentNumberIndex].State);
		//	//	CurrentNumberIndex++;
		//	//}

		//	highestTimeStamp = FMath::Max(TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp,
		//		ActionEvents[CurrentActionIndex].TimeStamp);

		//	//highestTimeStamp = FMath::Max3(TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp,
		//	//	ActionEvents[CurrentActionIndex].TimeStamp,
		//	//	NumberEvents[CurrentNumberIndex].TimeStamp);
		//	//highestTimeStampOfTwo = FMath::Max(StringEvents[CurrentStringIndex].TimeStamp, TransformEvents[CurrentTransformIndex].TimeStamp);
		//	//highestTimeStamp = FMath::Max(highestTimeStamp, highestTimeStampOfTwo);

		//}


		//// fire string events
		//for (int running = CurrentStringIndex; (running < StringEvents.Num()) && (currentTime > StringEvents[running].TimeStamp); running++, CurrentStringIndex++) {
		//	OnStringEventFiredDelegate.Broadcast(StringEvents[running].Id, StringEvents[running].Value, StringEvents[running].State);
		//}

		//// fire transform events
		//for (int running = CurrentTransformIndex; (running < TransformEvents.Num()) && (currentTime > TransformEvents[running].TimeStamp); running++, CurrentTransformIndex++) {
		//	OnTransformEventFiredDelegate.Broadcast(TransformEvents[running].Id, TransformEvents[running].Value, TransformEvents[running].IsRelative);
		//}


		// move tracked objects
		if (AutoTransformTrackedObjects && IsTracking) {
			for (; (CurrentTrackedTransformIndex < TrackedTransformEvents.Num()) && (currentTime > TrackedTransformEvents[CurrentTrackedTransformIndex].TimeStamp); CurrentTrackedTransformIndex++) {

				FSciprTransformEvent tracked_data = TrackedTransformEvents[CurrentTrackedTransformIndex];

				if (InterpolateTrackedObjectMovements) {
					FTransform target = tracked_data.Value;
					FTransform current = TrackedObjects[tracked_data.Id]->GetRelativeTransform();
					FTransform result = UKismetMathLibrary::TInterpTo(current, target, TimeSinceLastInterpolationStep, (float)PullingFrequency);
					TrackedObjects[tracked_data.Id]->SetRelativeTransform(result);
				}
				else {
					TrackedObjects[tracked_data.Id]->SetRelativeTransform(tracked_data.Value);
				}
			}

		}

		// fire action events
		for (; (CurrentActionIndex < ActionEvents.Num()) && (currentTime > ActionEvents[CurrentActionIndex].TimeStamp); CurrentActionIndex++) {
//			if (GEngine)
//				GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Actn Fired"));
			OnActionEventFired.Broadcast(ActionEvents[CurrentActionIndex].Id, ActionEvents[CurrentActionIndex].Value);
		}

		// fire number events
		for (int running = CurrentNumberIndex; (running < NumberEvents.Num()) && (currentTime > NumberEvents[running].TimeStamp); running++, CurrentNumberIndex++) {
			OnNumberEventFired.Broadcast(NumberEvents[running].Id, NumberEvents[running].Value, NumberEvents[running].State);
		}

		// fire string events
		for (int running = CurrentStringIndex; (running < StringEvents.Num()) && (currentTime > StringEvents[running].TimeStamp); running++, CurrentStringIndex++) {
			OnStringEventFired.Broadcast(StringEvents[running].Id, StringEvents[running].Value, StringEvents[running].State);
		}

		// fire transform events
		for (int running = CurrentTransformIndex; (running < TransformEvents.Num()) && (currentTime > TransformEvents[running].TimeStamp); running++, CurrentTransformIndex++) {
			OnTransformEventFired.Broadcast(TransformEvents[running].Id, TransformEvents[running].Value, TransformEvents[running].IsRelative);
		}

		if (IsPlaying && (currentTime > EndTime)) {
			this->StopPlayback();
		}

	}

}

void USciprInputRecordReplay::StartRecording(FString RecordingName, float TransformPullingFrequency, FString Path)
{
	if (IsRecording) {
		//UE_LOG(LogTemp, Warning, TEXT("Input recording started, before old one ended. Automatically closing old recording."));
		StopRecording();
	}

	this->StartTime = FPlatformTime::Seconds();
	this->Name = RecordingName;
	this->Filepath = Path;
	this->PullingFrequency = TransformPullingFrequency;

	MainJsonObject->SetStringField("recording_name", Name);
	MainJsonObject->SetNumberField("recording_frequency", TransformPullingFrequency);

	if (IsTracking) {
		this->GetOwner()->GetWorldTimerManager().SetTimer(RecordTrackedObjectsTimerHandle, this, &USciprInputRecordReplay::RecordTrackedObjects, (float) 1.0f / PullingFrequency, true);
	}

	this->IsRecording = true;
	OnStartRecording.Broadcast();
}

void USciprInputRecordReplay::StopRecording()
{

	// stop pulling transforms, if tracked objects are registered
	if (IsTracking) {
		this->GetOwner()->GetWorldTimerManager().ClearTimer(RecordTrackedObjectsTimerHandle);
	}

	// get recording length, and write it to file
	MainJsonObject->SetNumberField("recording_length", FPlatformTime::Seconds() - StartTime);

	///////////////////////////////////////////////////////////////////////////
	// serialize events

	// write out action events
	TArray<TSharedPtr<FJsonValue>> action_events;

	for (auto &event : ActionEvents) {

		TSharedRef<FJsonObject> eventJsonObj(new FJsonObject());
		FJsonObjectConverter::UStructToJsonObject(event.StaticStruct(), &event, eventJsonObj, 0, 0);
		TSharedPtr<FJsonValueObject> value = MakeShareable(new FJsonValueObject(eventJsonObj));
		action_events.Add(value);
	}
	MainJsonObject->SetArrayField("action_events", action_events);

	// write out number events
	TArray<TSharedPtr<FJsonValue>> number_events;

	for (auto &event : NumberEvents) {

		TSharedRef<FJsonObject> eventJsonObj(new FJsonObject());
		FJsonObjectConverter::UStructToJsonObject(event.StaticStruct(), &event, eventJsonObj, 0, 0);
		TSharedPtr<FJsonValueObject> value = MakeShareable(new FJsonValueObject(eventJsonObj));
		number_events.Add(value);
	}
	MainJsonObject->SetArrayField("number_events", number_events);

	// write out string events
	TArray<TSharedPtr<FJsonValue>> string_events;

	for (auto &event : StringEvents) {

		TSharedRef<FJsonObject> eventJsonObj(new FJsonObject());
		FJsonObjectConverter::UStructToJsonObject(event.StaticStruct(), &event, eventJsonObj, 0, 0);
		TSharedPtr<FJsonValueObject> value = MakeShareable(new FJsonValueObject(eventJsonObj));
		string_events.Add(value);
	}
	MainJsonObject->SetArrayField("string_events", string_events);

	// write out transform events
	TArray<TSharedPtr<FJsonValue>> transform_events;

	for (auto &event : TransformEvents) {

		TSharedRef<FJsonObject> eventJsonObj(new FJsonObject());
		FJsonObjectConverter::UStructToJsonObject(event.StaticStruct(), &event, eventJsonObj, 0, 0);
		TSharedPtr<FJsonValueObject> value = MakeShareable(new FJsonValueObject(eventJsonObj));
		transform_events.Add(value);
	}
	MainJsonObject->SetArrayField("transform_events", transform_events);

	// write out tracked transform events
	TArray<TSharedPtr<FJsonValue>> tracked_transform_events;

	for (auto &event : TrackedTransformEvents) {

		TSharedRef<FJsonObject> eventJsonObj(new FJsonObject());
		FJsonObjectConverter::UStructToJsonObject(event.StaticStruct(), &event, eventJsonObj, 0, 0);
		TSharedPtr<FJsonValueObject> value = MakeShareable(new FJsonValueObject(eventJsonObj));
		tracked_transform_events.Add(value);
	}
	MainJsonObject->SetArrayField("tracked_transform_events", tracked_transform_events);

	//////////////////////////////////////////////////////////////////////////////
	// write data to output string
	FString OutputString;
	TSharedRef< TJsonWriter<> > Writer = TJsonWriterFactory<>::Create(&OutputString);
	FJsonSerializer::Serialize(MainJsonObject.ToSharedRef(), Writer);

	// save string data to file
	//FString saveDir = FPaths::ConvertRelativePathToFull(FPaths::GameSavedDir());
	FString Filename = Filepath + "/" + Name + ".json";
	FPaths::NormalizeFilename(Filename);
	FFileHelper::SaveStringToFile(OutputString, *Filename);

	this->IsRecording = false;
	this->StartTime = 0.0;
	this->CurrentTime = 0.0;

	this->ActionEvents.Empty();
	this->NumberEvents.Empty();
	this->StringEvents.Empty();
	this->TransformEvents.Empty();

	OnStopRecording.Broadcast();
}

bool USciprInputRecordReplay::LoadFromString(FString Data) {
	TSharedRef< TJsonReader<> > JsonReader = TJsonReaderFactory<>::Create(Data);
	bool readSuccess = FJsonSerializer::Deserialize(JsonReader, MainJsonObject);

	if (!readSuccess)
		return false;

	this->Name = MainJsonObject->GetStringField("recording_name");
	this->PullingFrequency = MainJsonObject->GetNumberField("recording_frequency");
	this->EndTime = MainJsonObject->GetNumberField("recording_length");

	auto action_events = MainJsonObject->GetArrayField("action_events");
	FJsonObjectConverter::JsonArrayToUStruct(action_events, &ActionEvents, 0, 0);

	auto number_events = MainJsonObject->GetArrayField("number_events");
	FJsonObjectConverter::JsonArrayToUStruct(number_events, &NumberEvents, 0, 0);

	auto string_events = MainJsonObject->GetArrayField("string_events");
	FJsonObjectConverter::JsonArrayToUStruct(string_events, &StringEvents, 0, 0);

	auto transform_events = MainJsonObject->GetArrayField("transform_events");
	FJsonObjectConverter::JsonArrayToUStruct(transform_events, &TransformEvents, 0, 0);

	auto tracked_transform_events = MainJsonObject->GetArrayField("tracked_transform_events");
	FJsonObjectConverter::JsonArrayToUStruct(tracked_transform_events, &TrackedTransformEvents, 0, 0);

	return true;
}

void USciprInputRecordReplay::LoadFile(FString File, FString Path /* = "/" */, bool StartPlaybackAfterLoading) {
	FString Filename = Path + "/" + File + ".json";
	FPaths::NormalizeFilename(Filename);

	FString LoadGameStringData;

	FFileHelper::LoadFileToString(LoadGameStringData, *Filename);

	if (LoadFromString(LoadGameStringData)) {
		OnFileLoaded.Broadcast();

		if (StartPlaybackAfterLoading) {
			StartPlayback();
		}
	}
}

void USciprInputRecordReplay::LoadFileFromURL(FString URL) {
	// HTTP usage: https://github.com/EpicGames/UnrealEngine/blob/ab9713f2c0346236b0fdd271d3304cf0991d23de/Engine/Source/Runtime/Online/HTTP/Private/HttpTests.cpp
	// https://answers.unrealengine.com/questions/352833/download-a-file-to-hard-drive-blueprints-or-c.html
	// https://wiki.unrealengine.com/UE4.10_How_To_Make_HTTP_GET_Request_in_C%2B%2B
	//IHttpResponse::GetContentAsString();
	//FHttpModule* Http = &FHttpModule::Get();
	//TSharedPtr<IHttpRequest> Request = FHttpModule::Get().CreateRequest();
	//Request->OnProcessRequestComplete().BindSP(this, &UInputRecordReplay::RequestComplete);
	//Request->OnProcessRequestComplete().BindUObject(this, &USciprInputRecordReplay::RequestComplete);
	//Request->SetURL(URL);
	//Request->SetVerb(Verb);
	//Request->ProcessRequest();

}
void USciprInputRecordReplay::RequestComplete(FHttpRequestPtr HttpRequest, FHttpResponsePtr HttpResponse, bool bSucceeded) {
	if (!HttpResponse.IsValid())
	{
//		if (GEngine)
//			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Failed to load file from URL."));
	}
	else
	{
		if (LoadFromString(*HttpResponse->GetContentAsString())) {
			OnFileLoaded.Broadcast();
		}
	}
	HttpRequest->OnProcessRequestComplete().Unbind();
}

void USciprInputRecordReplay::StartPlayback() {
	this->IsPlaying = true;
	this->StartTime = FPlatformTime::Seconds();
	this->CurrentTime = 0.0;

	this->SetComponentTickEnabled(true);

	OnStartPlayback.Broadcast();
}

void USciprInputRecordReplay::StopPlayback()
{
	this->SetComponentTickEnabled(false);

	this->IsPlaying = false;
	this->StartTime = 0.0;

	this->CurrentTime = 0;
	this->CurrentActionIndex = 0;
	this->CurrentNumberIndex = 0;
	this->CurrentStringIndex = 0;
	this->CurrentTransformIndex = 0;
	this->CurrentTrackedTransformIndex = 0;

	OnStopPlayback.Broadcast();
}

float USciprInputRecordReplay::GetCurrentTime() const {
	return static_cast<float>((FPlatformTime::Seconds() - StartTime));
}

float USciprInputRecordReplay::GetEndTime() const {
	return static_cast<float>(EndTime);
}

void USciprInputRecordReplay::AddActionEvent(FString Identifier, bool Value)
{
	if (IsRecording) {
		ActionEvents.Add(FSciprActionEvent(FPlatformTime::Seconds() - StartTime, Identifier, Value));
	}
}

void USciprInputRecordReplay::AddActionEventPrepended(FString Identifier, bool Value, FString Prepend)
{
	if (IsRecording) {
		AddActionEvent(Prepend.Append(Identifier), Value);
	}
}

void USciprInputRecordReplay::AddNumberEvent(FString Identifier, float Value, bool State)
{
	if (IsRecording) {
		NumberEvents.Add(FSciprNumberEvent(FPlatformTime::Seconds() - StartTime, Identifier, Value, State));
	}
}

void USciprInputRecordReplay::AddStringEvent(FString Identifier, FString Value, bool State)
{
	if (IsRecording)
		if (IsRecording)StringEvents.Add(FSciprStringEvent(FPlatformTime::Seconds() - StartTime, Identifier, Value, State));
}

void USciprInputRecordReplay::AddTransformEvent(FString Identifier, FTransform Value, bool IsRelativeTransform)
{
	if (IsRecording)
		TransformEvents.Add(FSciprTransformEvent(FPlatformTime::Seconds() - StartTime, Identifier, Value, IsRelativeTransform));
}

void USciprInputRecordReplay::AddTrackedObject(FString Identifier, USceneComponent *ObjectToTrack)
{
	TrackedObjects.Add(Identifier, ObjectToTrack);
	if (!IsTracking && ObjectToTrack->IsValidLowLevel()) {
		IsTracking = true;
	}
}

void USciprInputRecordReplay::RecordTrackedObjects() {
	for (auto &tracked_objects : TrackedObjects) {
		FSciprTransformEvent transform_event(FPlatformTime::Seconds() - StartTime, tracked_objects.Key, tracked_objects.Value->GetRelativeTransform(), true);
		TrackedTransformEvents.Add(transform_event);
	}
}

//void UInputRecordReplay::PausePlayback(bool SetPause)
//{
//	if (SetPause) {
//		IsPlaying = false;
//		PrimaryComponentTick.SetTickFunctionEnable(false);
//	}
//	else {
//		IsPlaying = true;
//		PrimaryComponentTick.SetTickFunctionEnable(true);
//	}
//	
//}
